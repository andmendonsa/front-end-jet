/* Scripts Avalicacao */
$(document).on("input", '#test-name', function(e) {
    var input_value = this.value;

    var spanValue = document.getElementById("response-name");
	spanValue.innerText = input_value;
});

$(document).on("input", '#test-email', function(e) {
    var input_value = this.value;

    var spanValue = document.getElementById("response-email");
	spanValue.innerText = input_value;
});

$(document).on("input", '#test-phone', function(e) {
    var input_value = this.value;

    var spanValue = document.getElementById("response-phone");
	spanValue.innerText = input_value;
});

$(document).on("input", '#test-subject', function(e) {
    var input_value = this.value;

    var spanValue = document.getElementById("response-subject");
	spanValue.innerText = input_value;
});

$(document).on("input", '#test-message', function(e) {
    var input_value = this.value;

    var spanValue = document.getElementById("response-message");
	spanValue.innerText = input_value;
});

$(document).on("click", '#send-test-form', function(e) {
    e.preventDefault();

    $("#form-test").addClass('loading');

    var name = $("#test-name").val();
    var email = $("#test-email").val();
    var phone = $("#test-phone").val();
    var subject = $("#test-subject").val();
    var message = $("#test-message").val();

    sessionStorage.setItem('name', name);
    sessionStorage.setItem('email', email);
    sessionStorage.setItem('phone', phone);
    sessionStorage.setItem('subject', subject);
    sessionStorage.setItem('message', message);

    setTimeout(function(){ 
        $("#form-test").removeClass('loading');
        $("#form-test").addClass('success');
    }, 2000);
});

var test_name = sessionStorage.getItem("name");
var test_email = sessionStorage.getItem("email");
var test_phone = sessionStorage.getItem("phone");
var test_subject = sessionStorage.getItem("subject");
var test_message = sessionStorage.getItem("message");

$(document).ready(function() {
    if(test_name) {
        $("#form-content").addClass('show');
        $("#item-name").text('Nome: ' + test_name);
        $("#item-email").text('E-mail: ' + test_email);
        $("#item-phone").text('Telefone: ' + test_phone);
        $("#item-subject").text('Assunto: ' + test_subject);
        $("#item-message").text('Mensagem: ' + test_message);
    } else {
        $("#form-content").removeClass('show');
        $("#item-name").text('');
        $("#item-email").text('');
        $("#item-phone").text('');
        $("#item-subject").text('');
        $("#item-message").text('');
    }
});

$(document).on("click", '#form-content', function(e) {
    e.preventDefault();

    $(".content-form").fadeIn();
});
